import React, { Component } from "react";
import ProjectList from "../projects/ProjectList";
import Notification from "./Notification";
class Dashboard extends Component {
    state = {};
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-7 p-5">
                        <ProjectList />
                    </div>
                    <div className="col-md-5">
                        <Notification />
                    </div>
                </div>
            </div>
        );
    }
}

export default Dashboard;
