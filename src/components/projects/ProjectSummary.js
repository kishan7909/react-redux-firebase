import React, { Component } from "react";
import { Card, CardTitle, CardText } from "reactstrap";

class ProjectSummary extends Component {
    state = {};
    render() {
        return (
            <Card body className="mt-3">
                <CardTitle>
                    <h2>Project Title</h2>
                </CardTitle>
                <CardText>
                    <span>
                        Posted by Kishan Patel
                        <br />
                        <i style={{ color: "gray" }}>3rd Sep 2018</i>
                    </span>
                </CardText>
            </Card>
        );
    }
}

export default ProjectSummary;
