import React, { Component } from "react";
import { CardBody, CardTitle } from "reactstrap";

class ProjectDetails extends Component {
    state = {};
    render() {
        const id = this.props.match.params.id;
        console.log(id);
        return (
            <React.Fragment>
                <div className="container">
                    <div className="row">
                        <div className="col-md-8 ml-auto mr-auto mt-5">
                            <CardTitle>
                                <h2>Project Title</h2>
                            </CardTitle>
                            <CardBody>
                                <span>
                                    Lorem ipsum dolor, sit amet consectetur
                                    adipisicing elit. Sunt reprehenderit
                                    perspiciatis dolore, earum necessitatibus
                                    eius. Explicabo alias omnis deserunt
                                    quibusdam a eligendi? Reiciendis architecto,
                                    mollitia ipsa, culpa, numquam optio
                                    accusantium sapiente iusto vitae
                                    voluptatibus debitis quia expedita? Quod,
                                    maxime recusandae inventore nihil maiores
                                    saepe molestiae earum porro eius quo aperiam
                                    rem optio fugit sapiente. Eaque animi libero
                                    debitis quos itaque in doloribus id magni
                                    ipsam ratione totam non, aut harum ea! Omnis
                                    nihil deleniti ipsa veritatis distinctio in
                                    vitae molestias ducimus quas modi laborum,
                                    commodi unde enim totam doloremque veniam!
                                </span>
                                <hr />
                                <span style={{ color: "gray" }}>
                                    Posted By Kishan Patel<br />
                                    <i>3rd Sep 2018</i>
                                </span>
                            </CardBody>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default ProjectDetails;
