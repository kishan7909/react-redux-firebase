import React from "react";
import { NavLink as Link } from "react-router-dom";
import { NavItem, NavLink, Nav } from "reactstrap";
const SignedInLinks = () => {
    return (
        <Nav className="ml-auto" navbar>
            <NavItem>
                <NavLink tag={Link} to="/">
                    New Projects
                </NavLink>
            </NavItem>
            <NavItem>
                <NavLink tag={Link} className="mr-2" to="/">
                    Logout
                </NavLink>
            </NavItem>
            <NavItem>
                <NavLink
                    tag={Link}
                    className="btn btn-info btn-md"
                    style={{
                        borderRadius: "100%",
                        height: "2.5rem",
                        width: "2.5rem"
                    }}
                    to="/"
                >
                    KP
                </NavLink>
            </NavItem>
        </Nav>
    );
};

export default SignedInLinks;
