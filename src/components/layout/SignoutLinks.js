import React from "react";
import { NavLink as Link } from "react-router-dom";
import { NavItem, NavLink, Nav } from "reactstrap";
const SignoutLinks = () => {
    return (
        <Nav className="ml-auto" navbar>
            <NavItem>
                <NavLink tag={Link} to="/">
                    Signup
                </NavLink>
            </NavItem>
            <NavItem>
                <NavLink tag={Link} to="/">
                    Login
                </NavLink>
            </NavItem>
        </Nav>
    );
};

export default SignoutLinks;
