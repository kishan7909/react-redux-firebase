import React, { Component } from "react";
import { NavLink as Link } from "react-router-dom";
import { Collapse, Navbar, NavbarToggler, NavbarBrand } from "reactstrap";
import SignedInLinks from "./SignedInLinks";
import SignoutLinks from "./SignoutLinks";

class NavbarDesign extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    render() {
        return (
            <div>
                <Navbar color="dark" dark expand="sm">
                    <NavbarBrand tag={Link} to="/">
                        <h3>MarioPlan</h3>
                    </NavbarBrand>
                    <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <SignedInLinks />
                        <SignoutLinks />
                    </Collapse>
                </Navbar>
            </div>
        );
    }
}

export default NavbarDesign;
