import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import NavbarDesign from "./components/layout/NavbarDesign";
import Dashboard from "./components/dashboard/Dashboard";
import ProjectDetails from "./components/projects/ProjectDetails";

function NOTHING() {
    return <h1>404 - Page Not found</h1>;
}

class App extends Component {
    render() {
        return (
            <Router>
                <div>
                    <NavbarDesign />
                    <Switch>
                        <Route exact path="/" component={Dashboard} />
                        <Route
                            exact
                            path="/project/:id"
                            component={ProjectDetails}
                        />
                        <Route path="*" component={NOTHING} />
                    </Switch>
                </div>
            </Router>
        );
    }
}

export default App;
